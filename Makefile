LESS=$(wildcard source/client/*.less)
CSS=$(patsubst source/client%.less,resource%.css,$(LESS))
HTML_SOURCE=$(wildcard source/client/*.html)
HTML=$(patsubst source/client%.html,resource%.html,$(HTML_SOURCE))
AUTOPREFIX='last 2 Safari versions, last 2 iOS versions'
JS_SOURCE=$(wildcard source/client/*.js)
JS=resource/app.js


all:  resource $(HTML) $(CSS) $(JS)

resource:
	mkdir -p $@

resource/%.html:  source/client/%.html
	cp $< $@

resource/%.css:  source/client/%.less
	lessc $< --autoprefix=$(AUTOPREFIX) $@

resource/app.js:  $(JS_SOURCE)
	webpack
	#rollup --format=iife -- source/client/main.js \
	#	| babel --presets=es2016 \
	#	| uglifyjs --mangle --unsafe --compress=sequences,dead_code,conditionals,booleans,unused,if_return,join_vars,drop_console --screw-ie8 \
	#	> $@

install:  all
	true

preview:
	webpack-dev-server --host=0.0.0.0 --content-base=./resource

watch:
	while true; do make --silent; sleep 2; done
