///
///  User Controls
///
const node = require('./node');

function Dash(scope) {
    ///  Set up context
    ///
    if (!(this instanceof Dash)) {
        throw TypeError('constructor Dash requires `new`');
    }

    this.scope = scope;

    const loginCurtain = node('login', {}, [
        node('main', {}, [
            node('div', {}, [
                node('span', {}, 'prompt#1'),
                node('label', {}, 'email'),
                node('input', {type: 'email'}, []),
            ]),
            node('div', {}, [
                node('span', {}, 'prompt#2'),
                node('label', {}, 'secret'),
                node('input', {type: 'password'}, []),
            ]),
            node('div', {}, [
                node('span', {}, 'prompt#3'),
                node('label', {}, 'secret'),
                node('input', {type: 'password'}, []),
            ]),
        ]),
    ]);

    this.openKeyInputPane = loginCurtain.querySelector(
        'div:nth-of-type(1)');
    this.closedKeyInputPane = loginCurtain.querySelector(
        'div:nth-of-type(2)');
    this.closedKeyConfirmationPane = loginCurtain.querySelector(
        'div:nth-of-type(3)');

    this.openKeyControl =
        this.openKeyInputPane.querySelector('input');
    this.closedKeyControl =
        this.closedKeyInputPane.querySelector('input');
    this.closedKeyConfirmationControl =
        this.closedKeyConfirmationPane.querySelector('input');

    this.firstPrompt = this.openKeyInputPane.querySelector(
        'span');
    this.secondPrompt = this.closedKeyInputPane.querySelector(
        'span');
    this.thirdPrompt = this.closedKeyConfirmationPane.querySelector(
        'span');

    ///  one which gets `margin-left` assinged
    this.offsetBearer = this.openKeyInputPane;
    this.step = 0;
    this.lastStep = 1;
    this.readyToSumbit = false;

    this.firstPrompt.textContent =
        'You need to be logged in to edit this';

    Array.prototype.forEach.call(
        loginCurtain.querySelectorAll('input'),
        (element) => {
            element.addEventListener(
                'keyup', this.handleInput.bind(this));
        });

    if (self.localStorage.getItem('at') != null) {
        loginCurtain.style.display = 'none';
    }

    scope.appendChild(loginCurtain);
}

Dash.prototype.get = function() {
    const user = {};

    user.email = this.openKeyControl.value;
    user.secret = this.closedKeyControl.value;

    return user;
};

Dash.prototype.check = function() {
    ///  Is there such a user?

    fetch('/user/check', {
        method: 'POST',
        body: JSON.stringify(this.get()),
        credentials: 'omit'
    }).then((re) => re.json()).then((user) => {
        if (user.exists) {
            this.lastStep = 1;
            this.secondPrompt.textContent = 'Welcome back';
        } else {
            this.lastStep = 2;
            this.secondPrompt.textContent = 'Signing in as a new user';
            this.thirdPrompt.textContent = 'Same secret again';
        }

        this.readyToSumbit = true;
    });
};

Dash.prototype.advance = function() {
    if (this.step == this.lastStep) {
        return this.submit();
    }

    if (this.step == 0) {
        this.check();
    }

    this.step += 1;
    this.offsetBearer.style.marginLeft = `-${this.step * 100}%`;
};

Dash.prototype.recede = function() {
    if (this.step === 0) {
        return;
    }

    this.step -= 1;
    offsetBearer.style.marginLeft = `-${this.step * 100}%`;
};

Dash.prototype.submit = function() {
    if (!this.readyToSumbit) {
        return;
    }

    const user = this.get();
    if (this.step > 1) {
        if (user.secret !== this.closedKeyConfirmationControl.value) {
            return;
        }

        this.create(user);
    } else {
        this.auth(user);
    }

};

Dash.prototype.create = function(user) {
    fetch('/user/create', {
        method: 'POST',
        body: JSON.stringify(user),
        credentials: 'omit'
    }).then((re) => re.json()).then((re) => {
        if (re.success) {
            this.auth(user);
        }
    }).catch((error) => {
        console.error(error);
    });
};

Dash.prototype.auth = function(user) {
    fetch('/user/auth', {
        method: 'POST',
        body: JSON.stringify(user),
        credentials: 'omit'
    }).then((re) => re.json()).then((re) => {
        if (re.success && re.token) {
            self.localStorage.setItem('at', re.token);
            self.location.href = self.location.href;
        } else {
            console.error(re);
        }
    }).catch((error) => {
        console.error(error);
    });
};

Dash.prototype.handleInput = function(ev) {
    if (ev.keyCode === 13) {  /// mozilla says its deprecated
        this.advance();
    }
};

module.exports.attach = (scope) => new Dash(scope);
