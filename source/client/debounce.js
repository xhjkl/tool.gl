///
///  Call supplied function
///  only if a sufficient amount of time has elapsed
///  from the last invocation attempt
///

module.exports = function(milliseconds, callee) {
    var data = {
        timeoutId: null
    };

    const debounced = function() {
        clearTimeout(data.timeoutId);
        data.timeoutId = setTimeout(callee.bind(this), milliseconds);
    };

    return debounced;
};
