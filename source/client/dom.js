///
///  DOM manipulation utilities
///

function dropScript(source) {
    return new Promise();
}

function defile() {
    Array.prototype.forEach.call(
        document.querySelectorAll('*[pristine]'),
        (each) => each.removeAttribute('pristine'));
}

module.exports = { dropScript, defile };
