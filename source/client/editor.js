///
///  Wrapper around Ace
///

const aceCoreSource
    = '//cdnjs.cloudflare.com/ajax/libs/ace/1.2.2/ace.js';
const modeExtSource
    = '//cdnjs.cloudflare.com/ajax/libs/ace/1.2.2/mode-glsl.js';

function edit(element) {
    ///  Init editor

    ///  First, drop in necessary modules
    return new Promise((resolve, reject) => {
        var coreScript = document.createElement('script');
        coreScript.onload = resolve;
        coreScript.onerror = reject;
        coreScript.src = aceCoreSource;
        document.head.appendChild(coreScript);
    })
    .then(() => new Promise((resolve, reject) => {
        var modeScript = document.createElement('script');
        modeScript.onload = resolve;
        modeScript.onerror = reject;
        modeScript.src = modeExtSource;
        document.head.appendChild(modeScript);
    }))

    ///  After all is set up, invoke Ace initialisation
    .then(() => self.ace.edit(element))

    .catch(() => {
        console.error('could not load Ace');
    });
}

function read(editor) {
    ///  Provide all text that is currently
    ///  entered in a editing field
    return editor.getValue();
}

function focus(editor) {
    ///  Make editing area the active control
    ///
    editor.focus();
    return editor;
}

function configure(editor) {
    ///  Prepare the editor to work with

    editor.setTheme('ace/theme/monokai');
    editor.getSession().setMode('ace/mode/glsl');

    return editor;
}

function readonly(editor) {
    ///  Freeze
    editor.setReadOnly(true);
    return editor;
}

function readwrite(editor) {
    ///  Thaw
    editor.setReadOnly(false);
    return editor;
}

function change(editor, callback) {
    ///  Add action for when edited text changes
    editor.on('change', callback.bind(editor));

    return editor;
}

function boilerplate(editor, content) {
    ///  Add supplied text to the beginning of the editor field,
    ///  and make it uneditable
    ///
    ///  This should be called at most once
    ///
    const Range = self.require('ace/range').Range;
    const session = editor.getSession();

    function preventFromEditing(range) {
        return function(data, hash, keyString, keyCode, event) {
            if (hash === -1 || (37 <= keyCode && keyCode <= 40)) {
                ///  allow to move cursor
                return;
            }

            const selection = editor.getSelectionRange();
            if (selection.intersects(range)) {
                const start = selection.start;
                if (range.isEnd(start.row, start.column)) {
                    ///  the start of the modified selection
                    ///  mathes the end of this read-only range,
                    ///  so allow typing
                    if ([8].indexOf(keyCode) === -1) {
                        return;
                    }
                }
                return {command: 'null', passEvent: false};
            }
        };
    }

    // function handleReturn(data, hash, keyString, keyCode, event) {
    //     if (hash === 8 && keyCode === 13) {
    //         return true;
    //     }
    // }

    /// github.com/ajaxorg/ace/issues/2499
    editor.$blockScrolling = +Infinity;

    editor.setValue(content);
    const lines = session.getLength();

    const range = new Range(0, 0, lines, 0);
    editor.keyBinding.addKeyboardHandler({
        handleKeyboard: preventFromEditing(range)
    });
    const doOnPaste = editor.onPaste;
    editor.onPaste = function() {
        if (editor.getSelectionRange().intersects(range)) {
            return;
        }

        doOnPaste.apply(editor, arguments);
    };
    const doOnCut = editor.onCut;
    editor.onCut = function() {
        if (editor.getSelectionRange().intersects(range)) {
            return;
        }

        doOnCut.apply(editor, arguments);
    };

    session.addMarker(range, 'boilerplate');

    return editor
}

function preset(editor, content) {
    ///  Provision initial text to start with
    ///
    ///  Character sequence `;;` shall be removed,
    ///  and the cursor shall take its position
    ///
    editor.setValue(editor.getValue() + '\n' + content);
    editor.find(';;');
    editor.replace('');

    return editor;
}

module.exports = {
    edit, configure, readonly, readwrite, change,
    boilerplate, preset,
    focus, read
};
