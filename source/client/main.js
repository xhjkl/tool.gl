(function() {
const dom = require('./dom.js');
const dash = require('./dash.js');
const Shade = require('./shade.js');
const editor = require('./editor.js')
const debounce = require('./debounce.js');
const shaderInputs = [
    'uniform mediump float Time;',
    'uniform mediump vec2 Resolution;',
    'uniform sampler2D LastFrame;',
    'varying lowp vec2 Position;',
].join('\n');
const shaderFramework = [
    '',
    '',
    'void main()',
    '{',
    '    mediump vec4 last_color = texture2D(',
    '        LastFrame, gl_FragCoord.xy / Resolution);',
    '',
    '    mediump float t = Time / 256.0;',
    '    mediump vec2 center = vec2(',
    '        0.8 * cos(t),',
    '        0.3 * sin(2.0 * t)',
    '    );',
    '    mediump vec2 cc = Position - center;',
    '    lowp float ct = smoothstep(',
    '        0.1, 0.11, pow(cc.x * 2.0, 2.0) + pow(cc.y, 2.0));',
    '',
    '    gl_FragColor = mix(',
    '        vec4(',
    '            sin(312.0 + pow(t / 128.0, 2.0)),',
    '            0.7,',
    '            sin(128.0 + t / 480.0),',
    '            1.0',
    '        ),',
    '        vec4(last_color.rgb, last_color.a * 0.991),',
    '        ct',
    '    );',
    '}',
    '',
].join('\n');

addEventListener('DOMContentLoaded', () => {
    const style = document.createElement('link');
    style.setAttribute('rel', 'stylesheet');
    style.setAttribute('href', '/rich.css');
    document.head.appendChild(style);

    const canvas = document.querySelector('canvas');
    const gl = canvas.getContext('webgl');
    const shade = new Shade(gl);
    const recompileShader = debounce(128, function(delta) {
        shade.useSource(
            this.getValue(), console.error.bind(console));
    });
    const redraw = function(timestamp) {
        shade.render(timestamp);
        requestAnimationFrame(redraw);
    };

    dash.attach(document.body);

    editor.edit(
        document.querySelector('div[x-editable]'))
    .then(editor.readonly)
    .then(editor.configure)
    .then((field) => editor.boilerplate(field, shaderInputs))
    .then((field) => editor.preset(field, shaderFramework))
    .then((field) => {
        shade.useSource(
            field.getValue(), console.error.bind(console));
        return editor.change(field, recompileShader);
    })
    .then(editor.readwrite)
    .then(editor.focus)
    .then(dom.defile)
    .then(() => requestAnimationFrame(redraw));
});

}());
