///
///  Shortcut for creating elements
///

module.exports = (tag, attrs, content) => {
    attrs = attrs || {};
    content = content || [];

    const element = document.createElement(tag);
    Object.keys(attrs).forEach((key) => {
        element.setAttribute(key, attrs[key]);
    });
    if (Array.isArray(content)) {
        content.forEach((child) => {
            element.appendChild(child);
        });
    } else {
        element.textContent = content;
    }

    return element;
};
