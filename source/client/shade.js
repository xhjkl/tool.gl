///
///  WebGL wrapper for shaders manipulation
///

const passThroughVertexShaderSource = [
    'attribute vec2 position;',
    'varying vec2 Position;',
    'void main()',
    '{ gl_Position = vec4((Position = position), 0.0, 1.0); }',
].join('');
const passThroughFragmentShaderSource = [
    'uniform sampler2D texture;',
    'uniform mediump vec2 Resolution;',
    'void main() { gl_FragColor = texture2D(',
        'texture, gl_FragCoord.xy / Resolution); }',
].join('');

function Shade(gl) {
    ///  Set up context
    ///
    if (!(this instanceof Shade)) {
        throw TypeError('constructor Shade requires `new`');
    }

    this.gl = gl;
    this.width = 0;
    this.height = 0;
    this.unitQuadBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.unitQuadBuffer);
    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array([
            +1, -1,
            -1, -1,
            +1, +1,
            -1, +1,
        ]),
        gl.STATIC_DRAW
    );

    this.evenFrame = gl.createTexture();
    this.oddFrame = gl.createTexture();
    [this.evenFrame, this.oddFrame].forEach((texture) => {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(
            gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(
            gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(
            gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(
            gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    });

    this.evenFramebuffer = gl.createFramebuffer();
    this.oddFramebuffer = gl.createFramebuffer();
    [this.evenFramebuffer, this.oddFramebuffer].forEach((fb, i) => {
        gl.bindFramebuffer(gl.FRAMEBUFFER, fb);
        gl.framebufferTexture2D(
            gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
            gl.TEXTURE_2D, [this.evenFrame, this.oddFrame][i], 0);
    });

    this.prog = 0;
    this.frag = 0;
    this.vert = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(this.vert, passThroughVertexShaderSource);
    gl.compileShader(this.vert);
    if (!gl.getShaderParameter(this.vert, gl.COMPILE_STATUS)) {
        throw Error(
            'vertex shader failed to compile:'
            + ' ' + gl.getShaderInfoLog(this.vert)
        );
    }

    const passOnFrag = gl.createShader(gl.FRAGMENT_SHADER);
    this.passOn = gl.createProgram();
    gl.shaderSource(passOnFrag, passThroughFragmentShaderSource);
    gl.compileShader(passOnFrag);
    if (!gl.getShaderParameter(passOnFrag, gl.COMPILE_STATUS)) {
        throw Error(
            'pass-on shader failed to compile:'
            + ' ' + gl.getShaderInfoLog(passOnFrag));
    }

    gl.attachShader(this.passOn, this.vert);
    gl.attachShader(this.passOn, passOnFrag);
    gl.linkProgram(this.passOn);
    // gl.detachShader(this.passOn, this.vert);
    // gl.detachShader(this.passOn, passOnFrag);
    // gl.deleteShader(passOnFrag);
    if (!gl.getProgramParameter(this.passOn, gl.LINK_STATUS)) {
        throw Error(
            'pass-on program failed to link:'
            + ' ' + gl.getProgramInfoLog(this.passOn));
    }

    this.frameCount = 0;
    this.timeUniform = 0;
    this.resolutionUniform = 0;
    this.passOnResolutionUniform = gl.getUniformLocation(
        this.passOn, 'Resolution');

    gl.clearColor(0.0, 0.0, 0.0, 0.0);

    return this;
}

Shade.prototype.useSource = function(source, fail) {
    ///  Build and use a new program
    ///
    ///  Call back `fail` if the fragment shader
    ///  could not be compiled
    ///
    const gl = this.gl;
    const newProg = gl.createProgram();
    const newFrag = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(newFrag, source);
    gl.compileShader(newFrag);
    if(!gl.getShaderParameter(newFrag, gl.COMPILE_STATUS)) {
        fail(gl.getShaderInfoLog(newFrag));
        // gl.deleteShader(newFrag);
        return;
    }

    gl.attachShader(newProg, this.vert);
    gl.attachShader(newProg, newFrag);
    gl.linkProgram(newProg);
    if (!gl.getProgramParameter(newProg, gl.LINK_STATUS)) {
        fail(gl.getProgramInfoLog(newProg));
        // gl.deleteProgram(newProg);
        return;
    }

    if (this.prog) {
        // gl.deleteProgram(this.prog);
    }

    this.prog = newProg;
    this.frag = newFrag;
    this.timeUniform = gl.getUniformLocation(this.prog, 'Time');
    this.resolutionUniform = gl.getUniformLocation(
        this.prog, 'Resolution');
    // gl.detachShader(this.prog, this.vert);
    // gl.detachShader(this.prog, this.frag);
    // gl.deleteShader(this.frag);

    const attr = gl.getAttribLocation(this.prog, 'position');
    gl.enableVertexAttribArray(attr);
    gl.vertexAttribPointer(
        attr,
        2, gl.FLOAT, false, 2 * Float32Array.BYTES_PER_ELEMENT, 0);
}

Shade.prototype.adjust = function(width, height) {
    ///  Allocate texture storage so that the frame buffers
    ///  could contain the image to be rendered
    ///
    ///  Do nothing, if the buffers are already large enough
    ///
    if (width == this.width && height == this.height) {
        return;
    }

    const gl = this.gl;
    [this.evenFrame, this.oddFrame].forEach((texture) => {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(
            gl.TEXTURE_2D, 0,
            gl.RGBA, width, height,
            0,
            gl.RGBA, gl.UNSIGNED_BYTE, null);
    });
    gl.viewport(0, 0, width, height);

    this.width = width;
    this.height = height;
}

Shade.prototype.render = function(timestamp) {
    ///  Display the work of current shader
    ///  on the associated canvas
    ///
    const gl = this.gl;
    this.adjust(gl.canvas.clientWidth, gl.canvas.clientHeight);

    const odd = (this.frameCount & 1);
    const target = odd? this.oddFramebuffer: this.evenFramebuffer;
    const targetTexture = odd? this.oddFrame: this.evenFrame;
    const sourceTexture = odd? this.evenFrame: this.oddFrame;

    gl.bindFramebuffer(gl.FRAMEBUFFER, target);
    gl.bindTexture(gl.TEXTURE_2D, sourceTexture);
    gl.useProgram(this.prog);
    gl.uniform1f(this.timeUniform, timestamp);
    gl.uniform2f(
        this.resolutionUniform,
        gl.canvas.clientWidth, gl.canvas.clientHeight);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.bindTexture(gl.TEXTURE_2D, targetTexture);
    gl.useProgram(this.passOn);
    gl.uniform2f(
        this.passOnResolutionUniform,
        gl.canvas.clientWidth, gl.canvas.clientHeight);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    this.frameCount += 1;
}

module.exports = Shade;
