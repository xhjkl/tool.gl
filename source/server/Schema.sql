--  Persistent user data

create table user(
    id bigserial primary key not null
);

create table email(
    user_id references user(id) not null,
    address text unique not null
);

create table passcode(
    user_id references user(id) not null,
    secret bytea not null
);

-- create index passcode_secret_index on passcode(secret);

create table twitter(
    user_id references user(id) not null,
    handle text unique not null
);

create table github(
    user_id references user(id) not null,
    handle text unique not null
);

--  Volatile user journey data

create table journey(
    user_id references user(id) not null,
    level int not null default 0,
    token bytea primary key not null
);
