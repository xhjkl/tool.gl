#!/usr/bin/env python3
"""  Store and retrieve data
"""
import hmac
import random
import hashlib
import sqlite3


class Database(object):

    def __init__(self, name='user.db'):
        self._connection = sqlite3.connect(name)
        if self.__is_virgin():
            self.__rollout()

        # we need derived keys to have same length
        self.dummy_key = self.__derive_key('', '')

    def make_user_with_email_and_passcode(
        self, email, cleartext_passcode
    ):
        """  Make new user record
        """
        digest = self.__derive_key(email, cleartext_passcode)
        user_id = None

        with self._connection as connection:
            cursor = connection.cursor()
            user_id = self._make_user_id(cursor)
            cursor.execute(
                'insert into user(id) values(?)',
                (user_id,))
            cursor.execute(
                'insert into email(user_id, address) values(?, ?)',
                (user_id, email))
            cursor.execute(
                'insert into passcode(user_id, secret) values(?, ?)',
                (user_id, digest))
            connection.commit()

        return user_id

    def find_user_by_email(self, email):
        """  Tell if there a user with given email
        """
        cursor = self._connection.cursor()

        result = cursor.execute(
            'select user.id, email.address'
            '  from user'
            '    join email on email.user_id = user.id'
            '  where email.address = ?',
            (email,))
        fetch = result.fetchone()
        return (fetch is not None)

    def find_user_by_email_and_passcode(
        self, email, cleartext_passcode
    ):
        """  Return a tuple of (user id, email, secret)
        """
        cursor = self._connection.cursor()

        result = cursor.execute(
            'select user.id, email.address, passcode.secret'
            '  from user'
            '    join email on email.user_id = user.id'
            '    join passcode on passcode.user_id = user.id'
            '  where email.address = ?',
            (email,))
        fetch = result.fetchone()
        if fetch is not None:
            (user_id, email, secret) = fetch

            tentative_digest = self.__derive_key(
                email, cleartext_passcode)
            success = hmac.compare_digest(tentative_digest, secret)

            if success:
                return (user_id, email, secret)
            else:
                return None
        else:
            # reducing influence of timing side-channel
            tentative_digest = self.__derive_key(email, '')
            hmac.compare_digest(tentative_digest, self.dummy_key)

            return None

    def authorise_journey(self, user_id, privilege_level=0):
        """  Give the user a journey token
        """
        token = None

        with self._connection as connection:
            cursor = connection.cursor()

            token = self._make_journey_token(cursor)

            cursor.execute(
                'insert into journey(user_id, level, token)'
                ' values (?, ?, ?)',
                (user_id, privilege_level, token))

        return token

    def find_user_by_auth_token(self, token):
        """  Recall user id by a previously given journey token
        """
        cursor = self._connection.cursor()

        result = cursor.execute(
            'select user.id'
            '  from user'
            '    join journey on journey.user_id == user.id'
            '  where journey.token == ?',
            (token,))

        fetch = result.fetchone()
        if fetch is None:
            return None

        (user_id,) = fetch

        return user_id

    def _make_user_id(self, cursor):
        """  Return a large random number
             which is not currently occupied by a user id

             Execute within a transaction
        """
        attempts_cap = 1024
        for attempt in range(attempts_cap):
            user_id = random.randint(2 ** 11, 2 ** 29)

            for each in cursor.execute(
                'select id from user where id == ?', (user_id,)
            ):
                # we found someone, continue outer loop
                break
            else:
                # we did not break, id is vacant
                return user_id
        else:
            raise RuntimeError('entropy depleted')

    def _make_journey_token(self, cursor):
        """  Return an unoccupied sequence of bytes
             that shall identify a user's journey

             Execute within a transaction
        """
        attempts_cap = 1024
        for attempt in range(attempts_cap):
            token = bytes(
                random.randint(0, 255) for _ in range(64))

            for each in cursor.execute(
                'select token from journey where token == ?',
                (token,)
            ):
                break
            else:
                return token
        else:
            raise RuntimeError('entropy depleted')

    def __derive_key(self, email, cleartext_passcode):
        """  Work some magic so that thugs
             in their face-masks would be having a hard time
        """
        digest = hashlib.pbkdf2_hmac(
            'sha256',
            cleartext_passcode.encode('utf8'),
            email[::-1].encode('utf8'),
            4096
        )

        return digest

    def __rollout(self):
        """  Shape a virgin database by the baked-in schema
        """
        with open('source/server/Schema.sql') as f:
            sql = f.read()
        self._connection.executescript(sql)

    def __is_virgin(self):
        """  True iff there are no necessary tables
             regardless of their contents
        """
        cursor = self._connection.cursor()
        try:
            cursor.execute('select count(*) from user')
        except sqlite3.OperationalError:
            return True
        else:
            return False
