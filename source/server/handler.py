#!/usr/bin/env python3
"""  Respond to web requests
"""
import json
import base64
import os.path
import aiohttp.web
from ulog import log


class ApplicationContext(aiohttp.web.Application):
    """  All the necessary state
         to respond to the requests from the outside
    """

    NotFoundPage = '''
        <style>html,body{width:100%;height:100%;display:flex;}</style>
        <center style="margin:auto;
        font:bolder 404% sans-serif">404</center>\n\n'''

    StaticsDir = './resource'

    def __init__(self, *, db):
        super().__init__()

        self._db = db

        self.router.add_route('GET', '/lucky', self._lucky)
        self.router.add_route('POST', '/user/check', self._check_user)
        self.router.add_route('POST', '/user/create', self._add_user)
        self.router.add_route('POST', '/user/auth', self._auth)
        self.router.add_route('GET', '/{path:.*}', self._fallback)

    def _lucky(self, request):
        """  Make me lucky
        """
        return aiohttp.web.Response(body='<3'.encode('utf8'))

    async def _check_user(self, request):
        """  Let the client know if there is a user
             with specified handle
        """
        payload = None
        response = {'success': False}
        try:
            payload = await request.json()
        except Exception:
            return aiohttp.web.HTTPBadRequest(
                text='expected good JSON\n\n')

        if 'email' not in payload:
            return aiohttp.web.HTTPBadRequest(
                text='excepted field email\n\n')

        exists = self._db.find_user_by_email(payload['email'])
        response['exists'] = exists

        return aiohttp.web.Response(
            text=json.dumps(response),
            content_type='application/json')

    async def _auth(self, request):
        """  Grant access to a user
        """
        payload = None
        response = {'success': False}
        try:
            payload = await request.json()
        except Exception:
            return aiohttp.web.HTTPBadRequest(
                text='expected good JSON\n\n')

        expected_keys = {'email', 'secret'}
        if payload.keys() != expected_keys:
            return aiohttp.web.HTTPBadRequest(
                text='expected fields: {}\n\n'.format(expected_keys))

        row = self._db.find_user_by_email_and_passcode(
            payload['email'], payload['secret'])
        if row is not None:
            (user_id, _, _) = row
            bare_token = self._db.authorise_journey(user_id)
            response['token'] = self._encode_slug(user_id, bare_token)
            response['success'] = True

        return aiohttp.web.Response(
            text=json.dumps(response),
            content_type='application/json')

    async def _add_user(self, request):
        """  Record a new user to the database
        """
        payload = None
        response = {'success': False}
        try:
            payload = await request.json()
        except Exception:
            return aiohttp.web.HTTPBadRequest(
                text='excepted good JSON\n\n')

        expected_keys = {'email', 'secret'}
        if payload.keys() != expected_keys:
            return aiohttp.web.HTTPBadRequest(
                text='expected fields: {}\n\n'.format(expected_keys))

        user_id = self._db.make_user_with_email_and_passcode(
            payload['email'], payload['secret'])
        if user_id is not None:
            response['success'] = True

        return aiohttp.web.Response(
            text=json.dumps(response),
            status=202 if response['success'] else 400,
            content_type='application/json')

    def _fallback(self, request):
        """  Serve static, if possible
             Four-naught-four otherwise
        """
        path = self.__resolve_path(request.match_info['path'])
        content = None
        try:
            with open(path) as f:
                content = f.read()
        except (IsADirectoryError, FileNotFoundError):
            return aiohttp.web.HTTPNotFound(
                content_type='text/html; encoding=utf-8',
                body=self.NotFoundPage.encode('utf8'))

        return aiohttp.web.Response(body=content.encode('utf8'))

    def __resolve_path(self, path):
        """  Return a path to a static resource,
             shielded from directory traversal
        """
        actual_path = None
        resource_subpath = None
        if path in ('', '/', '.'):
            resource_subpath = 'main.html'
        else:
            # strip dot-dot
            normalised_path = os.path.normpath(
                os.path.join('/', path))

            # make '/xyz' into './xyz'
            resource_subpath = os.path.normpath(
                './' + normalised_path)

        actual_path = os.path.join(self.StaticsDir, resource_subpath)

        return actual_path

    @staticmethod
    def _encode_slug(user_id, bare_token):
        """  Take a user id and an auth token from database
             and return a base64-encoded byte sequence
             suitable for using in a browser cookie
        """
        raw_user_id = user_id.to_bytes(8, byteorder='little')
        slug = raw_user_id + bytes(bare_token)

        return base64.b64encode(slug).decode('utf8')

    @staticmethod
    def _decode_slug(slug):
        """  Take previously encoded cookie
             and split it back to user id and clean auth token
        """
        bytewise_token = base64.b64decode(slug.encode('utf8'))
        print('bytewise_token:', bytewise_token)
        user_id_part = bytewise_token[:8]
        auth_token_part = bytewise_token[8:]

        return (
            int.from_bytes(user_id_part, byteorder='little'),
            bytes(auth_token_part))
