#!/usr/bin/env python3
import aiohttp.web

import handler
import database


def main():
    db = database.Database()
    app = handler.ApplicationContext(db=db)

    aiohttp.web.run_app(app)

if __name__ == '__main__':
    main()
