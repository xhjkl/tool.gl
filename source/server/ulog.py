"""  Logging Micro-facility
"""
import os
from time import time, strftime, gmtime


def log(value=None, *formatting, **named_formatting):
    """  Record an event
    """
    if value is None:
        value = ''

    if isinstance(value, str):
        value = value.format(*formatting, **named_formatting)

    print('[{timestamp}.{ms:03} {pid:05}]  {msg}'.format(
        msg=value,
        timestamp=strftime('%y-%m-%d %H:%M:%S', gmtime()),
        ms=int(time() * 1000.0 % 1000),
        pid=os.getpid()))

__all__ = (log,)
