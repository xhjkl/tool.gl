#!/usr/bin/env python3
"""  Process data in background
"""
import time
import daemon
import asyncio
import traceback
from ulog import log


async def work():
    await asyncio.sleep(2)
    raise RuntimeError('nah')


async def main():
    log('worker started')
    last_start = time.time()
    while True:
        try:
            last_start = time.time()
            await work()
        except BaseException:
            log('error occurred:\n\n{}', traceback.format_exc())
        if time.time() - last_start < 1.0:
            log('crash rate too high; backing off')
            break
    log('will now shut down')

if __name__ == '__main__':
    logfile = open('/tmp/toolgl-work.log', 'a+')

    with daemon.DaemonContext(
        stdout=logfile, stderr=logfile
    ):
        asyncio.get_event_loop().run_until_complete(main())
