import sys
import pytest
sys.path.append('../source/server')
sys.path.append('source/server')
from database import Database  # nopep8


def test_user_creation():
    db = Database(':memory:')

    result = db.find_user_by_email_and_passcode(
        'me@w23.ru', 'likliklik')
    assert(result is None)

    result = db.make_user_with_email_and_passcode(
        'me@w23.ru', 'likliklik')
    assert(type(result) == int)

    result = db.find_user_by_email_and_passcode(
        'me@w23.ru', 'likliklik')
    assert(result is not None)


def test_journey_creation():
    db = Database(':memory:')

    user_id = db.make_user_with_email_and_passcode(
        'me@w23.ru', 'likliklik')
    token = db.authorise_journey(user_id)
    assert(type(token) == bytes)

    retrieved_user_id = db.find_user_by_auth_token(token)
    assert(retrieved_user_id == user_id)


if __name__ == '__main__':
    pytest.main(sys.argv)
