import sys
import pytest
sys.path.append('../source/server')
sys.path.append('source/server')
from handler import ApplicationContext  # nopep8


def test_cookie_storage():
    user_id = 0xFFEA
    auth_token = b'xv777'
    slug = ApplicationContext._encode_slug(user_id, auth_token)
    assert(type(slug) == str)

    unslug = ApplicationContext._decode_slug(slug)
    assert(unslug == (user_id, auth_token))


if __name__ == '__main__':
    pytest.main(sys.argv)
