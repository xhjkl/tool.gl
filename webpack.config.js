var webpack = require('webpack');
var path = require('path');


module.exports = {
	entry: 'client/main.js',
	resolve: {
		root: [path.resolve('source'), path.resolve('source/common')]
	},
	resolveLoader: {
		root: path.resolve(process.env['NODE_PATH'])
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			compress: {warnings: true}
		})
	],
	module: {
		loaders: [
			{ test: /\.js$/, exclude: 'node_modules', loader: 'babel-loader' }
		]
	},
	output: {
		path: path.resolve('./resource'),
		publicPath: '/',
		filename: 'app.js'
	}
};
